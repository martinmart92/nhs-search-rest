package fr.martin.babylon.natural;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.martin.babylon.model.SearchInputParameters;
import org.easymock.Mock;
import org.elasticsearch.client.Client;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by martin on 07/08/16.
 */
public class TranslatorTest {

    @Mock
    private Client client;

    @Test
    public void testTranslate() throws IOException {

        fillMockValues();

        assertEquals(new SearchInputParameters()
                        .addBmSection2("Asthma")
                        .addBmSection3("Symptoms"),
                Translator.translate("what are the symptoms of asthma?", this.client));

        assertEquals(new SearchInputParameters()
                        .addBmSection2("Cancer")
                        .addBmSection3("Symptoms"),
                Translator.translate("what are the symptoms of cancer?", this.client));


        /*assertEquals(new SearchInputParameters()
                        .addBmSection2("Headaches")
                        .addBmSection3("treatments"),
                Translator.translate("treatments for headaches", this.client));
                */
    }

    private void fillMockValues() throws IOException {
        ObjectMapper om = new ObjectMapper();

        Translator.allPossibleConcepts = om.readValue(
                this.getClass().getResourceAsStream("/possibleConcepts.json"), Set.class);

        Translator.allPossibleTypes = om.readValue(
                this.getClass().getResourceAsStream("/possibleTypes.json"), Set.class);

        Translator.allPossibleSection3 = om.readValue(
                this.getClass().getResourceAsStream("/possibleSection3.json"), Set.class);

        Translator.allPossibleSection2 = om.readValue(
                this.getClass().getResourceAsStream("/possibleSection2.json"), Set.class);
    }


    @Test
    public void testCleanSentence() {

        assertEquals("symptoms cancer", Translator.cleanSentence("what are the symptoms of cancer"));
        assertEquals("symptoms cancer", Translator.cleanSentence("what are the symptoms of cancer?"));
        assertEquals("symptoms cancer", Translator.cleanSentence("symptoms of cancer"));
        assertEquals("treatments headaches", Translator.cleanSentence("treatments for headaches"));
    }
}
