package fr.martin.babylon.model;

import org.junit.Test;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by martin on 07/08/16.
 */
public class SearchResultsTest {

    @Test
    public void testBean() {
        assertThat(SearchResults.class, allOf(
                hasValidBeanConstructor(),
                hasValidGettersAndSetters(),
                hasValidBeanHashCode(),
                hasValidBeanEquals(),
                hasValidBeanToString()
        ));
    }
}
