package fr.martin.babylon.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertEquals;

/**
 * Created by martin on 07/08/16.
 */
public class SearchInputParametersTest {

    @Test
    public void testBean() {
        assertThat(SearchInputParameters.class, allOf(
                hasValidBeanConstructor(),
                hasValidGettersAndSetters(),
                hasValidBeanHashCode(),
                hasValidBeanEquals(),
                hasValidBeanToString()
        ));
    }


    @Test
    public void testSerializeJSON() throws JsonProcessingException {

        SearchInputParameters params = new SearchInputParameters();
        params.getConcepts().add("concept1");
        params.getConcepts().add("concept2");
        params.getTypes().add("unique");
        params.getBmSection2().add("bmsection2");
        params.getBmSection3().add("bmsection3");
        params.getTitle().add("title1");
        params.getTitle().add("title2");

        ObjectMapper om = new ObjectMapper();
        String jsonStr = om.writeValueAsString(params);

        assertEquals("{\"concepts\":[\"concept2\",\"concept1\"],\"bmSection2\":[\"bmsection2\"],\"types\":[\"unique\"],\"bmSection3\":[\"bmsection3\"],\"title\":[\"title1\",\"title2\"]}",
                jsonStr );
    }

}
