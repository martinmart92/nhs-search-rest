package fr.martin.babylon;

/**
 * Created by martin on 07/08/16.
 */
public class Constants {

    //TODO transform these constants into runtime configurations parameters (like JVM params)

    public static final String ES_CLUSTER_NAME = "nhs-cluster";
    public static final String DEFAULT_FOLDER = "/home/martin/IdeaProjects/nhs-scraper/tmp/output";

    public static final String HOST = "localhost";
    public static final int PORT = 9300;

    public static final String INDEX_NAME = "nhs-conditions";
    public static final String INDEX_ENTRY_TYPE = "nhs-page";
    public static final String[] EXTENSIONS = new String[]{"json"};
    public static final String CROSS_ORIGIN = "http://localhost:9000";

}
