package fr.martin.babylon.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by martin on 07/08/16.
 */
public enum IndexFields {

    SECTION_TYPE("sectionType"),
    CONCEPT_NAME("conceptName"),
    TITLE("title"),
    URL("url"),
    //HTML("html"),
    UUID("uuid"),
    DESCRIPTION("description"),
    KEYWORDS("keywords"),
    BM_SECTION_1("bmsection1"),
    BM_SECTION_2("bmsection2"),
    BM_SECTION_3("bmsection3")
    ;


    private final String value;

    IndexFields(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


    public static String[] allAsStrArray() {
        List<String> listFields = Arrays.stream(IndexFields.values())
                .map(IndexFields::getValue)
                .collect(Collectors.toList());

        String[] arrayFields = new String[listFields.size()];
        return listFields.toArray(arrayFields);
    }
}
