package fr.martin.babylon.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by martin on 07/08/16.
 */
public class SearchInputParameters {

    private Set<String> concepts = new HashSet<>();
    private Set<String> bmSection2 = new HashSet<>();
    private Set<String> types = new HashSet<>();
    private Set<String> bmSection3 = new HashSet<>();
    private Set<String> title = new HashSet<>();

    public SearchInputParameters() {

    }

    public Set<String> getConcepts() {
        return concepts;
    }

    public void setConcepts(Set<String> concepts) {
        this.concepts = concepts;
    }

    public Set<String> getTypes() {
        return types;
    }

    public void setTypes(Set<String> types) {
        this.types = types;
    }

    public Set<String> getBmSection2() {
        return bmSection2;
    }

    public void setBmSection2(Set<String> bmSection2) {
        this.bmSection2 = bmSection2;
    }

    public Set<String> getBmSection3() {
        return bmSection3;
    }

    public void setBmSection3(Set<String> bmSection3) {
        this.bmSection3 = bmSection3;
    }


    public SearchInputParameters addBmSection2(String s) {
        getBmSection2().add(s);
        return this;
    }
    public SearchInputParameters addBmSection3(String s) {
        getBmSection3().add(s);
        return this;
    }

    public Set<String> getTitle() {
        return title;
    }

    public void setTitle(Set<String> title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchInputParameters that = (SearchInputParameters) o;

        if (concepts != null ? !concepts.equals(that.concepts) : that.concepts != null) return false;
        if (bmSection2 != null ? !bmSection2.equals(that.bmSection2) : that.bmSection2 != null) return false;
        if (types != null ? !types.equals(that.types) : that.types != null) return false;
        if (bmSection3 != null ? !bmSection3.equals(that.bmSection3) : that.bmSection3 != null) return false;
        return title != null ? title.equals(that.title) : that.title == null;

    }

    @Override
    public int hashCode() {
        int result = concepts != null ? concepts.hashCode() : 0;
        result = 31 * result + (bmSection2 != null ? bmSection2.hashCode() : 0);
        result = 31 * result + (types != null ? types.hashCode() : 0);
        result = 31 * result + (bmSection3 != null ? bmSection3.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchInputParameters{" +
                "concepts=" + concepts +
                ", bmSection2=" + bmSection2 +
                ", types=" + types +
                ", bmSection3=" + bmSection3 +
                ", title=" + title +
                '}';
    }
}
