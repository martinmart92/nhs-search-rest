package fr.martin.babylon.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by martin on 06/08/16.
 */
public class SearchResult {

    private Map<IndexFields, String> fields = new HashMap<>();

    public Map<IndexFields, String> getFields() {
        return fields;
    }

    public void setFields(Map<IndexFields, String> fields) {
        this.fields = fields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchResult that = (SearchResult) o;

        return fields != null ? fields.equals(that.fields) : that.fields == null;

    }

    @Override
    public int hashCode() {
        return fields != null ? fields.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "fields=" + fields +
                '}';
    }
}
