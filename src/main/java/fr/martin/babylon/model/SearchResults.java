package fr.martin.babylon.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 06/08/16.
 */
public class SearchResults {

    private int numberOfResults;

    List<SearchResult> results = new ArrayList<>();

    public int getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(int numberOfResults) {
        this.numberOfResults = numberOfResults;
    }

    public List<SearchResult> getResults() {
        return results;
    }

    public void setResults(List<SearchResult> results) {
        this.results = results;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchResults that = (SearchResults) o;

        if (numberOfResults != that.numberOfResults) return false;
        return results != null ? results.equals(that.results) : that.results == null;

    }

    @Override
    public int hashCode() {
        int result = numberOfResults;
        result = 31 * result + (results != null ? results.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchResults{" +
                "numberOfResults=" + numberOfResults +
                ", results=" + results +
                '}';
    }
}
