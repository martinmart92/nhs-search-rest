package fr.martin.babylon.controller;

import fr.martin.babylon.Constants;
import fr.martin.babylon.elasticsearch.BulkIndexer;
import fr.martin.babylon.elasticsearch.Searcher;
import fr.martin.babylon.model.SearchInputParameters;
import fr.martin.babylon.model.SearchResults;
import fr.martin.babylon.natural.Translator;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.plugin.deletebyquery.DeleteByQueryPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by martin on 06/08/16.
 */
@RestController
public class SearchController {

    private static final Logger LOG = LoggerFactory.getLogger(SearchController.class);

    private Client client;


    @PostConstruct
    public void init() throws UnknownHostException {
        createElasticSearchClient();
    }

    @PreDestroy
    public void cleanUp() throws Exception {
        closeElasticSearchClient();
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @RequestMapping("/index")
    public void index() throws IOException {
        LOG.info("index() start");
        createElasticSearchClient();

        BulkIndexer bulkIndexer = new BulkIndexer();
        LOG.debug("bulk indexing...");
        bulkIndexer.bulkIndexFolder(Constants.DEFAULT_FOLDER, this.client);
        Translator.clearCache();
        LOG.debug("bulk indexing done");
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @RequestMapping("/search-simple")
    public SearchResults searchSimple(@RequestParam(value = "input") String input) throws UnknownHostException {
        LOG.info("searchSimple() start with input : '" + input + "'");
        createElasticSearchClient();

        LOG.debug("searching...");
        Searcher searcher = new Searcher();
        SearchResults results = searcher.searchSimple(input, this.client);
        LOG.debug("searching done");
        return results;
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public SearchResults search(@RequestBody SearchInputParameters input) throws UnknownHostException {
        LOG.info("search() start with input : '" + input + "'");
        createElasticSearchClient();

        LOG.debug("searching...");
        Searcher searcher = new Searcher();
        SearchResults results = searcher.search(input, this.client);
        LOG.debug("searching done");
        return results;
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @RequestMapping("/search-natural")
    public SearchResults searchNatural(@RequestParam(value = "input") String input) throws IOException {
        LOG.info("searchNatural() start with input : '" + input + "'");
        createElasticSearchClient();

        LOG.debug("converting natural language to structured input parameter");
        SearchInputParameters translatedInput = Translator.translate(input, this.client);

        if ((translatedInput.getConcepts().isEmpty() && translatedInput.getBmSection2().isEmpty())
                || (translatedInput.getTypes().isEmpty() && translatedInput.getBmSection3().isEmpty())) {
            LOG.debug("searching first query may not find results, try another simpler one");
            translatedInput = Translator.translateSimpler(input, this.client);

        }
        LOG.debug("searching...");
        Searcher searcher = new Searcher();
        SearchResults results = searcher.search(translatedInput, this.client);
        LOG.debug("searching done");
        return results;
    }

    private Client createElasticSearchClient() throws UnknownHostException {

        LOG.info("creating elastic search client...");
        if (this.client != null) {
            LOG.debug("elastic search client already created");
            return this.client;
        }
        LOG.debug("creating new elastic search client");
        Settings settings = Settings.settingsBuilder()
                .put("cluster.name", Constants.ES_CLUSTER_NAME).build();

        this.client = TransportClient.builder()
                .settings(settings)
                .addPlugin(DeleteByQueryPlugin.class)
                .build()
                .addTransportAddress(new InetSocketTransportAddress(
                        InetAddress.getByName(Constants.HOST), Constants.PORT));

        LOG.info("creating elastic search client done");
        return this.client;

    }

    private void closeElasticSearchClient() {
        LOG.info("closing elastic search client...");
        this.client.close();
        LOG.info("closing elastic search client done");
    }
}
