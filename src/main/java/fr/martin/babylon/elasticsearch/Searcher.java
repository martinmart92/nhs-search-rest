package fr.martin.babylon.elasticsearch;

import fr.martin.babylon.model.IndexFields;
import fr.martin.babylon.model.SearchInputParameters;
import fr.martin.babylon.model.SearchResults;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import javax.annotation.Nonnull;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;

/**
 * Created by martin on 07/08/16.
 */
public class Searcher {


    /**
     * mostly a test method, to check if ES is searcheable
     * @param input
     * @param client
     * @return
     */
    @Nonnull
    public SearchResults searchSimple(@Nonnull String input, @Nonnull Client client) {
        //full-text query on all fields
        QueryBuilder qb = multiMatchQuery(
        input,
        IndexFields.allAsStrArray()
        );
        return buildQueryAndExecute(client, qb);
    }

    /**
     * search method based on input criteria
     * @param input
     * @param client
     * @return
     */
    @Nonnull
    public SearchResults search(@Nonnull SearchInputParameters input, @Nonnull Client client) {

        BoolQueryBuilder qb = boolQuery();
        //title
        if (input.getTitle()!=null && !input.getTitle().isEmpty()) {
            BoolQueryBuilder qbinner = boolQuery();
            qbinner.minimumNumberShouldMatch(1);

            for (String title : input.getTitle()) {
                qbinner.should(QueryBuilders.termQuery(IndexFields.TITLE.getValue(), title.toLowerCase()));
            }

            qb = qb.must(qbinner);
        }

        //concept
        if ((input.getConcepts()!=null && !input.getConcepts().isEmpty()
                || (input.getBmSection2()!=null && !input.getBmSection2().isEmpty()))) {
            BoolQueryBuilder qbinner = boolQuery();
            qbinner.minimumNumberShouldMatch(1);

            if (input.getBmSection2()!=null && !input.getBmSection2().isEmpty()) {
                for (String conceptName : input.getBmSection2()) {
                    qbinner.should(QueryBuilders.termQuery(IndexFields.BM_SECTION_2.getValue(), conceptName.toLowerCase()));
                }
            }
            else {
                for (String conceptName : input.getConcepts()) {
                    qbinner.should(QueryBuilders.termQuery(IndexFields.CONCEPT_NAME.getValue(), conceptName.toLowerCase()));
                }
            }
            qb = qb.must(qbinner);
        }
        //section type
        if ((input.getTypes()!=null && !input.getTypes().isEmpty())
        || (input.getBmSection3()!=null && !input.getBmSection3().isEmpty())) {
            BoolQueryBuilder qbinner = boolQuery();
            qbinner.minimumNumberShouldMatch(1);

            if (input.getBmSection3()!=null && !input.getBmSection3().isEmpty()) {

                for (String sectionType : input.getBmSection3()) {
                    qbinner.should(QueryBuilders.termQuery(IndexFields.BM_SECTION_3.getValue(), sectionType.toLowerCase()));
                }
            }
            else {
                for (String sectionType : input.getTypes()) {
                    qbinner.should(QueryBuilders.termQuery(IndexFields.SECTION_TYPE.getValue(), sectionType.toLowerCase()));
                }
            }

            qb = qb.must(qbinner);
        }
        return buildQueryAndExecute(client, qb);
    }


    /**
     * util method to build ES query and execute
     * @param client
     * @param qb
     * @return
     */
    @Nonnull
    private SearchResults buildQueryAndExecute(@Nonnull Client client, @Nonnull QueryBuilder qb) {
        SearchResponse response = client.prepareSearch()
                .setQuery(qb)
                .addFields(IndexFields.allAsStrArray())
                .execute()
                .actionGet();
        return Util.toSearchResults(response);
    }
}
