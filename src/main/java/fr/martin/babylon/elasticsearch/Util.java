package fr.martin.babylon.elasticsearch;

import fr.martin.babylon.model.IndexFields;
import fr.martin.babylon.model.SearchResult;
import fr.martin.babylon.model.SearchResults;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;

import javax.annotation.Nonnull;

/**
 * Created by martin on 07/08/16.
 */
public class Util {

    /**
     * from es to our internal data structure
     *
     * @param response
     * @return
     */
    public static SearchResults toSearchResults(@Nonnull SearchResponse response) {

        SearchResults results = new SearchResults();
        int count = 0;
        for (SearchHit hit : response.getHits().getHits()) {
            count++;

            SearchResult sr = new SearchResult();

            for (IndexFields field : IndexFields.values()) {

                if (hit.getFields().containsKey(field.getValue())) {
                    sr.getFields().put(
                            field,
                            hit.getFields().get(field.getValue()).getValue()
                    );
                }
            }
            results.getResults().add(sr);
        }
        results.setNumberOfResults(count);
        return results;
    }
}
