package fr.martin.babylon.elasticsearch;

import fr.martin.babylon.Constants;
import org.apache.commons.io.FileUtils;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by martin on 07/08/16.
 */
public class BulkIndexer {

    private static final Logger LOG = LoggerFactory.getLogger(BulkIndexer.class);

    private static final int BATCH_PAGE_FOR_LOADING = 100;

    /**
     * load all json files in a folder and directly index them into elasticsearch
     * @param folder
     * @param client
     * @throws IOException
     */
    public void bulkIndexFolder(@Nonnull String folder, @Nonnull Client client) throws IOException {


        long start = System.currentTimeMillis();
        LOG.info("bulk Index Folder : " + folder);
        emptyIndex(client);

        LOG.debug("listing files in folder...");
        Collection<File> jsonFiles = FileUtils.listFiles(new File(folder), Constants.EXTENSIONS, true);
        LOG.debug("listing files in folder found " + jsonFiles.size() + " files.");

        if (jsonFiles.size() < BATCH_PAGE_FOR_LOADING) {
            LOG.debug("direct bulk small size of files...");
            bulkIndex(jsonFiles, client);
        } else {
            LOG.debug("splitting file list into groups of " + BATCH_PAGE_FOR_LOADING + "...");

            List<Collection<File>> splitBatch = new ArrayList<>();

            List<File> currentSplit = new ArrayList<>();
            for (File f : jsonFiles) {
                if (currentSplit.size() == BATCH_PAGE_FOR_LOADING) {
                    splitBatch.add(currentSplit);
                    currentSplit = new ArrayList<>();
                }
                currentSplit.add(f);
            }
            if (currentSplit.size() != BATCH_PAGE_FOR_LOADING) {
                splitBatch.add(currentSplit);
            }

            int count = 1;
            for (Collection<File> splits : splitBatch) {
                LOG.debug("processing file group " + (count++) + "/" + splitBatch.size() + "...");
                bulkIndex(splits, client);
            }
        }

        afterIndex(client);
        long end = System.currentTimeMillis();
        LOG.info("bulk request finished in " + (end - start) + " ms.");
    }

    private void afterIndex(@Nonnull Client client) {
        LOG.debug("refreshing index...");
        client.admin().indices().prepareRefresh().get();
        LOG.debug("refreshing index done");
    }

    private void bulkIndex(Collection<File> jsonFiles, Client client) throws IOException {
        BulkRequestBuilder bulkRequest = client.prepareBulk();

        for (File jsonFile : jsonFiles) {

            String id = jsonFile.getName();
            bulkRequest.add(
                    client.prepareIndex(Constants.INDEX_NAME, Constants.INDEX_ENTRY_TYPE, id)
                            .setSource(
                                    FileUtils.readFileToByteArray(jsonFile)
                            )
            );
        }
        LOG.trace("executing bulk request...");
        BulkResponse bulkResponse = bulkRequest.get();
        if (bulkResponse.hasFailures()) {
            LOG.warn("executing bulk request has failures !!");
        }
    }


    private void emptyIndex(@Nonnull Client client) {

        LOG.info("empty Index...");

        if (!indexExists(Constants.INDEX_NAME, client)) {
            LOG.info("unable to empty index, does not exist yet.");
            return;
        }

        LOG.debug("empty Index processing delete by query...");//TODO change log text
        DeleteIndexResponse deleteResp = client.admin().indices()
                .delete(new DeleteIndexRequest(Constants.INDEX_NAME))
                .actionGet();
        if (!deleteResp.isAcknowledged()) {
            LOG.error("Index wasn't deleted");
        }
        else {
            LOG.info("Index was deleted.");

        }
        LOG.info("empty Index done");
    }


    public boolean indexExists(String indexName, Client client) {
        return client.admin().indices()
                .prepareExists(indexName)
                .execute().actionGet().isExists();
    }
}
