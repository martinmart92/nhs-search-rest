package fr.martin.babylon;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by martin on 06/08/16.
 */
@SpringBootApplication
public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);


    public static void main(String[] args) {
        LOG.info("run spring application...");
        if (args != null) {
            for (String arg : args) {
                LOG.debug("argument : " + arg);
            }
        }
        SpringApplication.run(Application.class, args);
        LOG.info("NHS Search REST Application is ready");
    }
}
