package fr.martin.babylon.natural;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.martin.babylon.elasticsearch.Util;
import fr.martin.babylon.model.IndexFields;
import fr.martin.babylon.model.SearchInputParameters;
import fr.martin.babylon.model.SearchResults;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

/**
 * util class to transform a free text sentence into a structured data structure
 * Created by martin on 07/08/16.
 */
public class Translator {

    private static final Logger LOG = LoggerFactory.getLogger(Translator.class);

    public static final String SPLIT_REGEX = " |\\?";

    protected static Set<String> allPossibleConcepts = new HashSet<>();
    protected static Set<String> allPossibleTypes = new HashSet<>();
    protected static Set<String> allPossibleSection2 = new HashSet<>();
    protected static Set<String> allPossibleSection3 = new HashSet<>();


    /**
     * heuristic-hard-coded parsing using two basic dictionaries
     * without any grammar understanding
     * @param sentence
     * @return
     */
    public static SearchInputParameters translate(@Nonnull String sentence, @Nonnull Client client) throws IOException {
        LOG.info("translate...");
        checkDictionariesLoaded(client);
        SearchInputParameters sip = new SearchInputParameters();

        sentence = cleanSentence(sentence);
        String[] words = sentence.split(SPLIT_REGEX);

        fillSet(words, allPossibleSection2, sip.getBmSection2());
        if (sip.getBmSection2().isEmpty()) {
            fillSet(words, allPossibleConcepts, sip.getConcepts());
        }
        fillSet(words, allPossibleSection3, sip.getBmSection3());
        if (sip.getBmSection3().isEmpty()) {
            fillSet(words, allPossibleTypes, sip.getTypes());
        }
        LOG.info("translated sentence : '"+sentence+"' into search input parameters : "+sip);
        return sip;
    }

    private static void fillSet(String[] words, Set<String> allPossibles, Set<String> output) {

        for (String word : words) {

            Optional<String> possibleWordOpt = allPossibles.parallelStream()
                    .filter(pw -> pw.toLowerCase().trim().equals(word))
                    .findFirst();

            if (possibleWordOpt.isPresent()) {
                output.clear();
                output.add(possibleWordOpt.get());
                return;
            }
            List<String> possibleWord = allPossibles.parallelStream()
                    .filter(pw -> pw.toLowerCase().trim().contains(word))
                    .collect(Collectors.toList());

            output.addAll(possibleWord);
        }
    }

    @Nonnull
    protected static String cleanSentence(@Nonnull String sentence) {

        sentence = sentence.trim().toLowerCase();

        if (sentence.contains("what are the ")) {
            sentence = sentence.replace("what are the ", "");
        }
        if (sentence.contains("of ")) {
            sentence = sentence.replace("of ", "");
        }
        if (sentence.contains("for ")) {
            sentence = sentence.replace("for ", "");
        }
        if (sentence.contains("?")) {
            sentence = sentence.replace("?", "");
        }
        return sentence;
    }


    private static void checkDictionariesLoaded(Client client) throws IOException {
        if (allPossibleConcepts.isEmpty() || allPossibleTypes.isEmpty() ||
        allPossibleSection2.isEmpty() || allPossibleSection3.isEmpty()) {
            loadDictionaries(client);
        }
    }
    public static void clearCache() {
        allPossibleConcepts.clear();
        allPossibleTypes.clear();
        allPossibleSection2.clear();
        allPossibleSection3.clear();
        LOG.info("cache cleared.");
    }
    private static void loadDictionaries(Client client) throws IOException {
        long start = System.currentTimeMillis();
        LOG.info("loading dictionaries...");
        clearCache();
        //reading from ES (query get all different values for fields)
        LOG.info("loading dictionaries : scroll all ES results");

        LOG.info("loading dictionaries : first query scroll");
        int count = 1;
        SearchResponse scrollResp = client.prepareSearch()
                //.addSort(SortParseElement.DOC_FIELD_NAME, SortOrder.ASC)
                .setScroll(new TimeValue(60000))
                .setQuery(matchAllQuery())
                .addFields(new String[] {
                        IndexFields.SECTION_TYPE.getValue(), IndexFields.CONCEPT_NAME.getValue(),
                        IndexFields.BM_SECTION_2.getValue(), IndexFields.BM_SECTION_3.getValue()
                })
                .setSize(100)
                .execute()
                .actionGet(); //100 hits per shard will be returned for each scroll

        //Scroll until no hits are returned
        while (true) {

            SearchResults results = Util.toSearchResults(scrollResp);

            results.getResults().parallelStream()
                    .forEach(r -> {
                        //type (e.g.: cause)
                        if (r.getFields().containsKey(IndexFields.BM_SECTION_3)) {
                            allPossibleSection3.add(r.getFields().get(IndexFields.BM_SECTION_3).trim());
                        }
                        if (r.getFields().containsKey(IndexFields.SECTION_TYPE)) {
                            allPossibleTypes.add(r.getFields().get(IndexFields.SECTION_TYPE).trim());
                        }
                        //concept (e.g.: cancer)
                        if (r.getFields().containsKey(IndexFields.BM_SECTION_2)) {
                            allPossibleSection2.add(r.getFields().get(IndexFields.BM_SECTION_2).trim());
                        }
                        if (r.getFields().containsKey(IndexFields.CONCEPT_NAME)) {
                            allPossibleConcepts.add(r.getFields().get(IndexFields.CONCEPT_NAME).trim());
                        }
                    });
            LOG.debug("loading dictionaries : query scroll ("+(count+1)+")...");
            scrollResp = client.prepareSearchScroll(scrollResp.getScrollId())
                    .setScroll(new TimeValue(60000))
                    .execute()
                    .actionGet();
            //Break condition: No hits are returned
            if (scrollResp.getHits().getHits().length == 0) {
                break;
            }
            count++;
        }
        LOG.info("loading dictionaries : done "+count+" queries scroll");
        LOG.info("loading dictionaries : all possible types size : "+allPossibleTypes.size());
        LOG.info("loading dictionaries : all possible section 3 size : "+allPossibleSection3.size());
        LOG.info("loading dictionaries : all possible concept size : "+allPossibleConcepts.size());
        LOG.info("loading dictionaries : all possible section 2 size : "+allPossibleSection2.size());
        /*tmpOutputSet(allPossibleTypes, "possibleTypes.json");
        tmpOutputSet(allPossibleConcepts, "possibleConcepts.json");
        tmpOutputSet(allPossibleSection2, "possibleSection2.json");
        tmpOutputSet(allPossibleSection3, "possibleSection3.json");
        */

        LOG.info("loading dictionaries : use ES results done. Time : "+(System.currentTimeMillis()-start)+" ms.");
    }

    private static void tmpOutputSet(Set<String> set, String filename) {

        ObjectMapper om = new ObjectMapper();
        try {
            om.writeValue(new File(filename), set);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nonnull
    public static SearchInputParameters translateSimpler(@Nonnull String sentence, @Nonnull Client client) {
        sentence = cleanSentence(sentence);
        String[] words = sentence.split(SPLIT_REGEX);

        SearchInputParameters searchParams = new SearchInputParameters();
        for (String word : words) {
            searchParams.getTitle().add(word);
        }
        return searchParams;
    }
}
