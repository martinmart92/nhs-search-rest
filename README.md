# NHS Search Rest #

## Description

Spring based Rest service API, in Java, integrated with Elasticsearch 2.3.5 to index scrap output from nhs-scraper (https://bitbucket.org/martinmart92/nhs-scraper)

##dependency
elasticsearc 2.3.5

* download elasticsearch and install into : {location}/elasticsearch-2.3.5/

* cd {location}/elasticsearch-2.3.5/bin

* install plugin head : ./plugin -install mobz/elasticsearch-head

* run : ./elasticsearch

* URL : http://localhost:9200/_plugin/head/


##compile 

```
mvn clean install
```

##run : 
```
java -jar target/nhs-search-rest-1.0-SNAPSHOT.jar
```

##input:

this module reads a list of json files in a hard-coded path (change it in the Constant java files)

##available REST endpoints:


* index: 
* * (re-)index scrapped files
* * path : GET /index
* * example : http://localhost:8080/index


* search-natural
* * main search with natural language sentence
* * path : GET /search-natural
* * example: http://localhost:8080/search-natural?input=treatments for headaches


* search-simple
* * simple keyword search (testing purposes)
* * path: GET /search-simple?input={keyword}
* * example: http://localhost:8080/search-simple?input=test


* search
* * search endpoint
* * path : POST /search
* * example : http://localhost:8080/search
* * request header: Content-Type : application/json